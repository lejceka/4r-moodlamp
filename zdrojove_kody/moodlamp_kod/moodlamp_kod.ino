#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>
#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>

#define DHTPIN D6
#define DHTTYPE DHT11

#define LED_PIN D1
#define NUM_LEDS 47
#define LED_TYPE NEO_GRB + NEO_KHZ800

const char* ssid = "3301-IoT";
const char* password = "mikrobus";
const char* mqtt_server = "broker.hivemq.com";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE (200)
char msg[MSG_BUFFER_SIZE];
int value = 0;

DHT dht(DHTPIN, DHTTYPE);
Adafruit_NeoPixel strip(NUM_LEDS, LED_PIN, LED_TYPE);

bool isMoodModeActive = false;
uint32_t currentMoodColor = strip.Color(255, 255, 255); // Default color

void setup_wifi()
{
    delay(10);
    // Připojení k WiFi síti
    Serial.println();
    Serial.print("Připojování k síti ");
    Serial.println(ssid);

    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    randomSeed(micros());

    Serial.println("");
    Serial.println("WiFi připojeno");
    Serial.println("IP adresa: ");
    Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
    client.loop();

    Serial.print("Přijata zpráva [");
    Serial.print(topic);
    Serial.print("] ");

    String message = "";
    for (int i = 0; i < length; i++) {
        message += (char)payload[i];
    }
    Serial.println(message);

    if (String(topic) == "lejcek/moodlamp/strip/modes") {
        // Mode handling remains the same
        if (message == "1") {
            setMoodColor(strip.Color(255, 255, 255)); // Daylight
        } else if (message == "2") {
            setMoodColor(strip.Color(0, 255, 255));   // Neon Lights
        } else if (message == "3") {
            setMoodColor(strip.Color(255, 69, 0));    // Sunset
        } else if (message == "0") {
            setMoodColor(strip.Color(0, 0, 0));      // Turn off
        }
    } else if (String(topic) == "lejcek/moodlamp/strip/rgb") {
        // Extract RGB values from message string
        int startIndex = message.indexOf('(') + 1;
        int endIndex = message.indexOf(')');
        String rgbString = message.substring(startIndex, endIndex);
        int commaIndex1 = rgbString.indexOf(',');
        int commaIndex2 = rgbString.indexOf(',', commaIndex1 + 1);

        // Parse individual RGB values
        int r = rgbString.substring(0, commaIndex1).toInt();
        int g = rgbString.substring(commaIndex1 + 1, commaIndex2).toInt();
        int b = rgbString.substring(commaIndex2 + 1).toInt();

        // Set mood color
        setMoodColor(strip.Color(r, g, b));
    }
}

void setMoodColor(uint32_t color)
{
    currentMoodColor = color;
    if (!isMoodModeActive)
    {
        strip.fill(color, 0, NUM_LEDS);
        strip.show();
    }
}

void reconnect()
{
    // Pokus o opětovné připojení k MQTT brokeru
    while (!client.connected())
    {
        Serial.print("Pokus o připojení k MQTT...");
        // Vytvoření náhodného ID klienta
        String clientId = "ESP8266Client-";
        clientId += String(random(0xffff), HEX);
        // Pokus o připojení
        if (client.connect(clientId.c_str()))
        {
            Serial.println("připojeno");
            // Po připojení publikovat oznámení...
            client.publish("lejcek/moodlamp/test", "hello world");
            // ... a opětovně se přihlásit k odběru zpráv
            client.subscribe("lejcek/moodlamp/Node-RED");
            client.subscribe("lejcek/moodlamp/strip/modes");
            client.subscribe("lejcek/moodlamp/strip/rgb");
        }
        else
        {
            Serial.print("selhalo, kód chyby=");
            Serial.print(client.state());
            Serial.println(" Zkusím to znovu za 5 sekund");
            // Počkat 5 sekund před dalším pokusem
            delay(5000);
        }
    }
}

void setup()
{
    Serial.begin(115200);

    dht.begin();
    strip.begin();
    strip.show();
    setup_wifi();
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);
}

void loop()
{
    if (!client.connected())
    {
        reconnect();
    }
    client.loop();

    unsigned long now = millis();
    static unsigned long lastMsg = 0;
    if (now - lastMsg > 2000)
    {
        lastMsg = now;
        float humidity = dht.readHumidity();
        float temperature = dht.readTemperature();
        if (isnan(humidity) || isnan(temperature))
        {
            Serial.println("Chyba při čtení z DHT senzoru!");
            return;
        }

        client.publish("lejcek/moodlamp/temperature", String(temperature).c_str());
        client.publish("lejcek/moodlamp/humidity", String(humidity).c_str());
    }

    // Aktivace módu nálady na základě příchozí zprávy
    if (isMoodModeActive)
    {
        for (int i = 0; i < strip.numPixels(); i++)
        {
            strip.setPixelColor(i, currentMoodColor);
        }
        strip.show();
        delay(50); // Úprava zpoždění podle potřeby
    }
}

