# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  |16 hodin|
| odkud jsem čerpal inspiraci                   |https://www.instructables.com/RGB-Lamp-WiFi/|
| odkaz na video                                |https://youtu.be/f0uwSx02Q-Y|
| jak se mi to podařilo rozplánovat             |Vcelku dobře, stihl jsem všechny datumy a stihl jsem celou práci dodělat v předstihu|
| proč jsem zvolil tento design                 |Tento design mi přišel takový nejpřirozenější a nejjednodušší|
| zapojení                                      |https://gitlab.spseplzen.cz/lejceka/4r-moodlamp/-/blob/main/dokumentace/schema/image.png|
| z jakých součástí se zapojení skládá          | AISHI ERS1VM101E12OT 100uF ±20% 35V kondenzátor elektrolytický, GeB Li-Ion Baterie 18650 3200mAh 3.7V, Nabíječka Li-ion článku TP4056 s ochranou miniUSB, WeMos D1 Mini Pro 16MB ESP8266 WiFi modul, Led pásek Neopixel WS2813 60led/m černá, Senzor teploty a vlhkosti DHT11 a dále spousta vodičů.|
| realizace                                     |https://gitlab.spseplzen.cz/lejceka/4r-moodlamp/-/blob/main/dokumentace/fotky/IMG_20240212_164438.jpg|
| UI                                            |https://gitlab.spseplzen.cz/lejceka/4r-moodlamp/-/blob/main/dokumentace/fotky/ui.PNG|
| co se mi povedlo                              |Design|
| co se mi nepovedlo/příště bych udělal/a jinak |Nic|
| zhodnocení celé tvorby (návrh známky)         |Řekl bych, že jsem splnil vše správně takže 1|